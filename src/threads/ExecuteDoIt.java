package threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteDoIt {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(2);

        threads.DoIt ds1 = new threads.DoIt('+', 26, 1000);
        threads.DoIt ds2 = new threads.DoIt('-', 10, 500);
        threads.DoIt ds3 = new threads.DoIt('*', 5, 250);
        threads.DoIt ds4 = new threads.DoIt('/', 2, 100);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);

        myService.shutdown();
    }
}