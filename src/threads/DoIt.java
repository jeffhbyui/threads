package threads;

import java.util.Random;

public class DoIt implements Runnable {

    private final char mathmatic;
    private int number;
    private int rand;
    private int timeout;

    public DoIt(char mathmatic, int number, int timeout) {
        this.mathmatic = mathmatic;
        this.number = number;
        this.timeout = timeout;


        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {
        System.out.println("\n\nExecuting: " + number + mathmatic + rand + "\n\n");
        for (int count = 1; count < number; count++) {
            if (count % number <= 3) {
                System.out.print("\n" + number + mathmatic + rand + " timeout... \n");
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        if(mathmatic == '+'){
            System.out.println("\nResult of " + number + mathmatic + rand + ": " + (number + rand) + "\n");
        } else if (mathmatic == '-') {
            System.out.println("\nResult of " + number + mathmatic +  rand + ": " +  (number - rand) + "\n");
        } else if (mathmatic == '*') {
            System.out.println("\nResult of " + number + mathmatic + rand + ": " +  (number * rand) + "\n");
        } else if (mathmatic == '/') {
            System.out.println("\nResult of " + number + mathmatic +  rand + ": " +  (number / rand) + "\n");
        }
    }
}